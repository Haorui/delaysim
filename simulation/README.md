Network simulation with NS-3, version ns-3.29:
https://www.nsnam.org/documentation/


NS-3 installation follows:
https://www.nsnam.org/wiki/Installation

Only tested on Ubuntu 16.04
Simulation in cpp works on MAC OSX but got problem with python binding (will take a look again)

NS-3 Waf as build system:
https://waf.io/

Simulation scripts should ran from ns-3 source tree:

*  For cpp scripts:
   ` ./waf --run $(DIR)/script`

*  For python scripts:
   ` ./waf --pyrun $(DIR)/script.py`

If want to run script out of ns-3 source tree, add teh following command in ~/.bashrc

```
export NS3DIR=~/source/ns-3.29
function waff { 
    MYPATH=$(pwd)
    FILE="$2"
    CMD="$1"
    echo "file directory: $MYPATH/$FILE"
    cd $NS3DIR && ./waf $CMD $MYPATH/$FILE; 
    cd $MYPATH
}
```
Then run the script from current working directory with `waff`
