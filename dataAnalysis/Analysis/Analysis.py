import numpy as np
import matplotlib as mpl
mpl.use('TkAgg')
import matplotlib.pyplot as plt
import random
import math
from sklearn import mixture

class Delay:
    def __init__(self, name):
        self.name = name
        self.data = []

class Measurements:
    def __init__(self, data_path, display=None):
        self.path = data_path
        self.measurements = Delay('Measurements')  # list of delay data
        self.clean = Delay('Filtered Measurements')  # list of filtered delay data
        self.log = Delay('Log(Measurements)') # list of logarithm of raw delay data
        self.clog = Delay('Log(Filtered Measurements)') # list of logarithm of filtered delay data
        self.alpha = 0.98  # Default Ratio of outliars
        self.outliar_removed = False
        f = open(self.path, 'r')
        for line in f.readlines():
            d = float(line.split("\t")[1])
            self.measurements.data.append(d)
        f.close()
        self.plotter = plotDist()
        if display==True:
            self.plotter.plotHist(self.measurements.name+" Histogram", self.measurements.data)

    # Remove outliars with ratio alpha, default 0.98 by default
    def RemoveOutliar(self, alpha=None, display=None):
        if alpha is not None:
            self.alpha=alpha
        tmp_delay = np.array(self.measurements.data)
        lim = int(math.ceil(self.alpha * tmp_delay.size))-1
        #TODO: Refactor to replace the conditional statement
        if lim == -1:
            val = np.sort(tmp_delay, axis=None)[0]
            indxl = np.argwhere(tmp_delay>=val)
        else:
            val = np.sort(tmp_delay, axis=None)[lim]
            indxl = np.argwhere(tmp_delay>val)
        self.clean.data = np.delete(tmp_delay, indxl)
        if display is not None:
            self.plotter.plotHist(self.clean.name+" Histogram", self.clean.data)
        self.outliar_removed = True
        return self.clean

    # Calculate the logarithm for delay array
    def CalcLog(self, display=None):
        self.log.data = np.log(self.measurements.data)
        if display is not None:
            self.plotter.plotHist(self.log.name+" Histogram", self.log.data)
        if self.outliar_removed:
            self.clog.data = np.log(self.clean.data)
            if display is not None:
                self.plotter.plotHist(self.clog.name+" Histogram", self.clog.data)
            return (self.log, self.clog)
        else:
            return (self.log,)


class FitGMM:
    def __init__(self, meas, modes=None):
        if modes is not None:
            self.n_modes = modes
        else:
            self.n_modes = 5
        self.name = meas.name
        self.data = meas.data
        self.gmm = mixture.GaussianMixture(n_components=self.n_modes, covariance_type='full')
        self.training_data = self.data.reshape(-1,1)
        self.gmm.fit_predict(self.training_data)
        self.pdf = []

    def getMean(self):
        return self.gmm.means_

    def getCovar(self):
        return self.gmm.covariances_

    def getPDF(self, display=None):
        X = np.linspace(np.amin(self.training_data), np.amax(self.training_data), 1000).reshape(1000, 1)
        logprob = self.gmm.score_samples(X)
        self.pdf = np.exp(logprob)
        if display is not None:
            self.PlotPDF(X)
        return self.pdf

    def PlotPDF(self, X):
        plt.figure(self.name+ " Histogram", figsize=(8, 5)) #Plot the PDF on the corresponding histogram
        plt.plot(X, self.pdf, color='C3')
        plt.show(block=False)

    #pull out 500 samples randomly from rhe fitted distribution and compare with the original measuremnts
    def PlotCompare(self, display=None):
        start = random.randint(1, np.shape(self.training_data)[0])
        stop = start + 500
        mea_ = self.training_data[start:stop]  #measurements takens from the training data
        gen_= self.gmm.sample(500)[0]
        gen_ = np.reshape(gen_, gen_.size)
        np.random.shuffle(gen_)
        if display is not None:
            plt.figure("Compare", figsize=(8,5))
            plt.subplot(211)
            plt.plot(mea_, 'o--', linewidth=0.3, markersize=2)
            plt.subplot(212)
            plt.plot(gen_, 'o--',linewidth=0.3, markersize=2, color='C2')
            plt.show(block=False)

class plotDist:
    def __init__(self):
        self.nbins = 500

    def plotHist(self, name, data, nbins=None):
        if nbins is not None:
            self.nbins = nbins
        plt.figure(name, figsize=(8,5))
        #xmin, xmax = plt.xlim()
        #plt.xlim(5, 40)
        plt.hist(data, self.nbins, density=True)
        plt.show(block=False)

    def plotMultiHist(self, name, nbins=None, *data):
        if nbins is not None:
            self.nbins = nbins
        N = len(data) #get number of figure name to plot
        plt.figure(name, figsize=(8,5))
        n = 1
        for d in data:
            plt.subplot(N1n)
            plt.hist(d, self.nbins, density=True)
        plt.show(block=False)

