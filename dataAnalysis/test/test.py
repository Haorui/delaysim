import unittest
import test_FitGMM
import test_Measurements


loader = unittest.TestLoader()
suite = unittest.TestSuite()


suite.addTests(loader.loadTestsFromModule(test_Measurements))
suite.addTests(loader.loadTestsFromModule(test_FitGMM))

runner = unittest.TextTestRunner(verbosity=2)
result = runner.run(suite)