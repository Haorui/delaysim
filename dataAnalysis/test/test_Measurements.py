import unittest
from Analysis.Analysis import Measurements
from Analysis.Analysis import Delay

class MeasurementsTestCase(unittest.TestCase):
	
	#test the measurements is correctly stored in a list
	def test_Init_(self): 
		data_path = "../data/data.txt"
		Mea = Measurements(data_path)
		self.assertGreater("The measurements list can't be empty", len(Mea.measurements.data), 0)

	def test_RemoveOutliar_NoRemoval(self):
		data_path = "../data/data.txt"
		Mea = Measurements(data_path)
		self.assertEquals(len(Mea.RemoveOutliar(alpha=1).data), len(Mea.measurements.data), "No removals when alpha is one")

	def test_RemoveOutliar_AllRemoval(self):
		data_path = "../data/data.txt"
		Mea = Measurements(data_path)
		self.assertEquals(len(Mea.RemoveOutliar(alpha=0).data), 0, "All data should be removed if alpha is 0")

	def test_CalcLog_OneOutput_NoRemoveOutliarsCall(self):
		data_path = "../data/data.txt"
		Mea = Measurements(data_path)
		self.assertEquals(len(Mea.CalcLog()), 1, "If RemoveOutliar() is not called, there is only one output")

	def test_CalcLog_TwoOutput_NoRemoveOutliarsCall(self):
		data_path = "../data/data.txt"
		Mea = Measurements(data_path)
		clean_delay = Mea.RemoveOutliar()
		self.assertEquals(len(Mea.CalcLog()), 2, "If RemoveOutliar() is called, there should be two outputs")
		lean_delay = Mea.RemoveOutliar(alpha=1)
		self.assertEquals(len(Mea.CalcLog()), 2, "If RemoveOutliar() is called, there should be two outputs, even if alpha=1")
		lean_delay = Mea.RemoveOutliar(alpha=0)
		self.assertEquals(len(Mea.CalcLog()), 2, "If RemoveOutliar() is called, there should be two outputs, even if alpha=0")
