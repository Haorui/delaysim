import unittest
import pickle
from Analysis.Analysis import FitGMM, Measurements


class FitGMMTestCase(unittest.TestCase):
	@classmethod
	def setUpClass(cls):
		data_path = "test/testdata.txt"
		Mea = Measurements(data_path)
		delay_in = Mea.RemoveOutliar()
		cls.delay = delay_in
		cls.GMM = FitGMM(delay_in)
		cls.GMM_threemodes = FitGMM(delay_in, modes=3)
		# cls.delay = Delay(delay_in.name)
		# cls.delay.data = delay_in.data

	def test_Init_(self):
		self.assertEquals(self.GMM.n_modes, 5, "number of modes should be 5 by default")
		self.assertEquals(self.GMM_threemodes.n_modes, 3, "number of modes should change if mode arguments passed")

	def test_MeanValue(self):
		self.assertGreater(max(self.delay.data), max(self.GMM.getMean()), "The means values should be less than the maximun vallue of teh measurements")
		self.assertGreater(max(self.delay.data), max(self.GMM_threemodes.getMean()), "The means values should be less than the maximun vallue of teh measurements")

	def test_Covariance(self):
		self.assertEquals(len(self.GMM.getCovar()), self.GMM.n_modes, "length of covariance array should be the same as modes")
		self.assertEquals(len(self.GMM_threemodes.getCovar()), self.GMM_threemodes.n_modes, "length of covariance array should be the same as modes")

	def test_getPDF(self):
		self.assertEquals(len(self.GMM.getPDF()), 1000, "The number of returned pdf values should always be 1000")
		self.assertEquals(len(self.GMM_threemodes.getPDF()), 1000, "The number of returned pdf values should always be 1000")
